import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class AccesDAO {

	/**
	 * Param�tres de connexion � la base de donn�es oracle
	 * URL, LOGIN et PASS sont des constantes
	 */
	final static String URL = "jdbc:oracle:thin:@localhost:1521:xe";
	final static String LOGIN="bdd5";
	final static String PASS="bdd5";


	/**
	 * Constructeur de la classe
	 * 
	 */
	public AccesDAO()
	{
		// chargement du pilote de bases de donn�es
		try {
			Class.forName("oracle.jdbc.OracleDriver");
		} catch (ClassNotFoundException e2) {
			System.err.println("Impossible de charger le pilote de BDD, ne pas oublier d'importer le fichier .jar dans le projet");
		}

	}
	

	/**
	 * Permet d'ajouter un acces dans la table acces
	 * la r�f�rence de l'acces est produite automatiquement par la base de donn�es en utilisant une s�quence
	 * Le mode est auto-commit par d�faut : chaque insertion est valid�e
	 * @param nouvAcces l'acces a ajouter
	 * @return le nombre de ligne ajout�es dans la table
	 */
	public int ajouter(Acces nouvAcces)
	{
		Connection con = null;
		PreparedStatement ps = null;
		int retour=0;

		//connexion � la base de donn�es
		try {

			//tentative de connexion
			con = DriverManager.getConnection(URL, LOGIN, PASS);
			//pr�paration de l'instruction SQL, chaque ? repr�sente une valeur � communiquer dans l'insertion
			//les getters permettent de r�cup�rer les valeurs des attributs souhait�s de nouvArticle
			ps = con.prepareStatement("INSERT INTO acces (acc_id, acc_motif, acc_date, acc_per_id) VALUES (acces_seq.NEXTVAL, ?, ?, ?)");
			ps.setString(1,nouvAcces.getMotif());
			ps.setString(2,nouvAcces.getDate());
			ps.setString(3,nouvAcces.getPer_id());

			//Ex�cution de la requ�te
			retour=ps.executeUpdate();


		} catch (Exception ee) {
			ee.printStackTrace();
		} finally {
			//fermeture du preparedStatement et de la connexion
			try {if (ps != null)ps.close();} catch (Exception t) {}
			try {if (con != null)con.close();} catch (Exception t) {}
		}
		return retour;

	}

	/**
	 * Permet de r�cup�rer un acces � partir de sa r�f�rence
	 * @param id l'id de l'acces � r�cup�rer
	 * @return l'acces
	 * @return null si aucun acces ne correspond � cette r�f�rence
	 */
	public Acces getAcces(String id)
	{

		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs=null;
		Acces retour=null;

		//connexion � la base de donn�es
		try {

			con = DriverManager.getConnection(URL, LOGIN, PASS);
			ps = con.prepareStatement("SELECT * FROM acces WHERE acc_id = ?");
			ps.setString(1,id);

			//on ex�cute la requ�te
			//rs contient un pointeur situ� jusute avant la premi�re ligne retourn�e
			rs=ps.executeQuery();
			//passe � la premi�re (et unique) ligne retourn�e 
			if(rs.next())
				retour=new Acces(rs.getString("acc_id"),rs.getString("acc_motif"),rs.getString("acc_date"),rs.getString("acc_per_id"));


		} catch (Exception ee) {
			ee.printStackTrace();
		} finally {
			//fermeture du ResultSet, du PreparedStatement et de la Connection
			try {if (rs != null)rs.close();} catch (Exception t) {}
			try {if (ps != null)ps.close();} catch (Exception t) {}
			try {if (con != null)con.close();} catch (Exception t) {}
		}
		return retour;

	}

	/**
	 * Permet de r�cup�rer tous les acces stock�s dans la table acces
	 * @return une ArrayList d'Articles
	 */
	public List<Acces> getListeAcces()
	{

		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs=null;
		List<Acces> retour=new ArrayList<Acces>();

		//connexion � la base de donn�es
		try {

			con = DriverManager.getConnection(URL, LOGIN, PASS);
			ps = con.prepareStatement("SELECT * FROM acces");

			//on ex�cute la requ�te
			rs=ps.executeQuery();
			//on parcourt les lignes du r�sultat
			while(rs.next())
				retour.add(new Acces(rs.getString("acc_id"),rs.getString("acc_motif"),rs.getString("acc_date"),rs.getString("acc_per_id")));


		} catch (Exception ee) {
			ee.printStackTrace();
		} finally {
			//fermeture du rs, du preparedStatement et de la connexion
			try {if (rs != null)rs.close();} catch (Exception t) {}
			try {if (ps != null)ps.close();} catch (Exception t) {}
			try {if (con != null)con.close();} catch (Exception t) {}
		}
		return retour;

	}

	//main permettant de tester la classe
	public static void main(String[] args)  throws SQLException {

		 AccesDAO accesDAO=new AccesDAO();
		//test de la m�thode ajouter
		Acces a = new Acces("fdhvc45","etude","10/03/2015","2");
		int retour=accesDAO.ajouter(a);

		System.out.println(retour+ " lignes ajout�es");

		//test de la m�thode getArticle
		Acces a2 = accesDAO.getAcces("1");
		System.out.println(a2);

		 //test de la m�thode getListeArticles
		List<Acces> liste=accesDAO.getListeAcces();
		//System.out.println(liste);
		for(Acces acc : liste)
		{
			System.out.println(acc.toString());
		}

	}

}
