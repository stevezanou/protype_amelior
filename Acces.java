
public class Acces {

	/** 
	 * id de l'acces
	 */
	private String id;		
	/**
	 * motif de l'acces
	 */
	private String motif;	
	/**
	 * date de l'utilisation de l'acces
	 */
	private String date;
	/**
	 * foreign key qui associe l'acces a la personne
	 */
	private String per_id;
	
	


	/**
	 * Constructeur
	 * @param id id de l'acces
	 * @param motif motif de l'acces
	 * @param date date de l'utilisation de l'acces
	 * @param per_id foreign key qui associe l'acces a la personne
	
	 */
	public Acces(String id, String motif, String date,String per_id) {
		this.id=id;
		this.motif=motif;
		this.date=date;
		this.per_id=per_id;
		
	}
	/**
	 * Constructeur - l'id n'est pas fixée dans le programme
	 * @param motif motif de l'acces
	 * @param date date de l'utilisation de l'acces
	 * @param per_id foreign key qui associe l'acces a la personne
	*/
	public Acces( String motif, String date,String per_id) {
		this.motif=motif;
		this.date=date;
		this.per_id=per_id;
		
	}
	
	
	/**
	 * Constructeur - l'id n'est pas fixée dans le programme
	 * @param date date de l'utilisation de l'acces
	 * @param per_id foreign key qui associe l'acces a la personne
	 */
	public Acces(  String date,String per_id) {

		this.date=date;
		this.per_id=per_id;
		
	}
	
	
	/**
	 * getter pour l'attribut id
	 * @return valeur de l'id de l'acces
	 */
	public String getId() {
		return id;
	}
	/**
	 * getter pour l'attribut motif
	 * @return motif de l'acces
	 */
	public String getMotif() {
		return motif;
	}
	/**
	 * setter  pour l'attribut motif
	 * @param motif : nouveau motif de l'acces
	 */
	public void setMotif(String motif) {
		this.motif = motif;
	}
	/**
	 * getter  pour l'attribut date
	 * @return valeur de la date
	 */
	public String getDate() {
		return date;
	}
	/**
	 * setter  pour l'attribut date
	 * @param date :  nouvelle valeur de la date
	 */
	public void setDate(String date) {
		this.date = date;
	}
	/**
	 * getter  pour l'attribut per_id
	 * @return valeur de l'id de la personne associee
	 */
	public String getPer_id() {
		return per_id;
	}
	/**
	 * setter  pour l'attribut per_id
	 * @param per_id :  nouvelle valeur de l'id de la personne associee
	 */
	public void setPer_id(String per_id) {
		this.per_id = per_id;
	}
	

	/**
	 * Redéfinition de la méthode toString permettant de définir la traduction de l'objet en String
	 * pour l'affichage par exemple
	 */
	public String toString() {
		return "Acces Id : " + id + ", motif: " + motif+ ", date: "+ date+ " personne associée: "+per_id; 
	}
}
