import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class PersonneDAO {
	
	/**
	 * Param�tres de connexion � la base de donn�es oracle
	 * URL, LOGIN et PASS sont des constantes
	 */
	final static String URL = "jdbc:oracle:thin:@localhost:1521:xe";
	final static String LOGIN="bdd5";
	final static String PASS="bdd5";


	/**
	 * Constructeur de la classe
	 * 
	 */
	public PersonneDAO()
	{
		// chargement du pilote de bases de donn�es
		try {
			Class.forName("oracle.jdbc.OracleDriver");
		} catch (ClassNotFoundException e2) {
			System.err.println("Impossible de charger le pilote de BDD, ne pas oublier d'importer le fichier .jar dans le projet");
		}

	}
	

	/**
	 * Permet d'ajouter une personne dans la table personne 
	 * l'id de la personne est produit automatiquement par la base de donn�es en utilisant une s�quence
	 * Le mode est auto-commit par d�faut : chaque insertion est valid�e
	 * @param nouvPersonne la personne a ajouter
	 * @return le nombre de ligne ajout�es dans la table
	 */
	public int ajouter(Personne nouvPersonne)
	{
		Connection con = null;
		PreparedStatement ps = null;
		int retour=0;

		//connexion � la base de donn�es
		try {

			//tentative de connexion
			con = DriverManager.getConnection(URL, LOGIN, PASS);
			//pr�paration de l'instruction SQL, chaque ? repr�sente une valeur � communiquer dans l'insertion
			//les getters permettent de r�cup�rer les valeurs des attributs souhait�s de nouvArticle
			ps = con.prepareStatement("INSERT INTO personne (per_id, per_nom, per_prenom) VALUES (personne_seq.NEXTVAL, ?, ?)");
			ps.setString(1,nouvPersonne.getNom());
			ps.setString(2,nouvPersonne.getPrenom());
			

			//Ex�cution de la requ�te
			retour=ps.executeUpdate();


		} catch (Exception ee) {
			ee.printStackTrace();
		} finally {
			//fermeture du preparedStatement et de la connexion
			try {if (ps != null)ps.close();} catch (Exception t) {}
			try {if (con != null)con.close();} catch (Exception t) {}
		}
		return retour;

	}

	/**
	 * Permet de r�cup�rer une personne à partir de son id
	 * @param id l'id de la personne à r�cup�rer
	 * @return la personne
	 * @return null si aucune personne ne correspond � cet id
	 */
	public Personne getPersonne(String id)
	{

		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs=null;
		Personne retour=null;

		//connexion � la base de donn�es
		try {

			con = DriverManager.getConnection(URL, LOGIN, PASS);
			ps = con.prepareStatement("SELECT * FROM personne WHERE per_id = ?");
			ps.setString(1,id);

			//on ex�cute la requ�te
			//rs contient un pointeur situ� jusute avant la premi�re ligne retourn�e
			rs=ps.executeQuery();
			//passe � la premi�re (et unique) ligne retourn�e 
			if(rs.next())
				retour=new Personne(rs.getString("per_id"),rs.getString("per_nom"),rs.getString("per_prenom"));


		} catch (Exception ee) {
			ee.printStackTrace();
		} finally {
			//fermeture du ResultSet, du PreparedStatement et de la Connection
			try {if (rs != null)rs.close();} catch (Exception t) {}
			try {if (ps != null)ps.close();} catch (Exception t) {}
			try {if (con != null)con.close();} catch (Exception t) {}
		}
		return retour;

	}

	/**
	 * Permet de r�cup�rer toutes les personnes stock�s dans la table personne
	 * @return une ArrayList de Personnes
	 */
	public List<Personne> getListePersonnes()
	{

		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs=null;
		List<Personne> retour=new ArrayList<Personne>();

		//connexion � la base de donn�es
		try {

			con = DriverManager.getConnection(URL, LOGIN, PASS);
			ps = con.prepareStatement("SELECT * FROM personne");

			//on ex�cute la requ�te
			rs=ps.executeQuery();
			//on parcourt les lignes du r�sultat
			while(rs.next())
				retour.add(new Personne(rs.getString("per_id"),rs.getString("per_nom"),rs.getString("per_prenom")));


		} catch (Exception ee) {
			ee.printStackTrace();
		} finally {
			//fermeture du rs, du preparedStatement et de la connexion
			try {if (rs != null)rs.close();} catch (Exception t) {}
			try {if (ps != null)ps.close();} catch (Exception t) {}
			try {if (con != null)con.close();} catch (Exception t) {}
		}
		return retour;

	}

	//main permettant de tester la classe
	public static void main(String[] args)  throws SQLException {

		 PersonneDAO personneDAO=new PersonneDAO();
		//test de la m�thode ajouter
		Personne a = new Personne("François","Paul");
		int retour=personneDAO.ajouter(a);

		System.out.println(retour+ " lignes ajout�es");

		//test de la m�thode getArticle
		Personne a2 = personneDAO.getPersonne("id");
		System.out.println(a2);

		 //test de la m�thode getListeArticles
		List<Personne> liste=personneDAO.getListePersonnes();
		//System.out.println(liste);
		for(Personne per : liste)
		{
			System.out.println(per.toString());
		}

	}

}
