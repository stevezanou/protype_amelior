
public class Personne {
	
	
	/** 
	 * id de la personne
	 */
	private String id;		
	/**
	 * nom de la personne
	 */
	private String nom;	
	/**
	 * prenom de la personne
	 */
	private String prenom;		
	


	/**
	 * Constructeur
	 * @param id id de la personne
	 * @param nom nom de la personne
	 * @param prenom prenom de la personne
	
	 */
	public Personne(String id, String nom, String prenom) {
		this.id=id;
		this.nom= nom;
		this.prenom = prenom;
		
	}
	/**
	 * Constructeur - l'id n'est pas fixée dans le programme
	 * @param nom nom de la personne
	 * @param prenom prenom de la personne
	
	 */
	public Personne( String nom, String prenom) {
		this.nom=nom;
		this.prenom=prenom;
		
	}
	
	/**
	 * getter pour l'attribut id
	 * @return valeur de l'id de la personne
	 */
	public String getId() {
		return id;
	}
	/**
	 * getter pour l'attribut nom
	 * @return valeur du nom de la personne
	 */
	public String getNom() {
		return nom;
	}
	/**
	 * setter  pour l'attribut nom
	 * @param nom : nouvelle valeur du nom de la personne
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}
	/**
	 * getter  pour l'attribut prenom
	 * @return valeur du prenom
	 */
	public String getPrenom() {
		return prenom;
	}
	/**
	 * setter  pour l'attribut prenom
	 * @param prenom :  nouvelle valeur du prenom
	 */
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	

	/**
	 * Redéfinition de la méthode toString permettant de définir la traduction de l'objet en String
	 * pour l'affichage par exemple
	 */
	public String toString() {
		return "Personne Id : " + id + ", Nom: " + nom+ ", Prenom: "+ prenom; 
	}
}

	



