

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.Dimension;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JPanel;
import javax.swing.BoxLayout;
import javax.swing.Box;

import java.util.List;


public class PersonneFenetre extends JFrame implements ActionListener {
	
	/**
	 * numero de version pour classe serialisable
	 * Permet d'eviter le warning "The serializable class ArticleFenetre does not declare a static final serialVersionUID field of type long"
	 */
	private static final long serialVersionUID = 1L; 
	
	/**
	 * conteneur : il accueille les differents composants graphiques de ArticleFenetre
	 */
	private JPanel containerPanel;		
	
	/**
	 * zone de texte pour le champ designation
	 */
	private JTextField textFieldNom;	
	
	/**	
	 * zone de texte pour le prix unitaire hors taxe
	 * 	     	
	 */
	private JTextField textFieldPrenom;
	
	/**
	 * label id
	 */
	private JLabel labelId;	 			
	
	/**
	 * label nom
	 */
	private JLabel labelNom; 		
	
	/**
	 * label prenom
	 */
	private JLabel labelPrenom;	     		
	
	 		
	/**
	 * bouton d'envoi de la personne
	 */
	private JButton boutonEnvoi;	 	
	/**
	 *  bouton qui permet d'afficher tous les personnes
	 */
	private JButton boutonAffichageTousLesPersonnes;	
	/**
	 * Zone de texte pour afficher les personnes
	 */
	JTextArea zoneTextListPersonnes;
	/**
	 * Zone de défilement pour la zone de texte
	 */
	JScrollPane zoneDefilement;
	/**
	 * instance de PersonneDAO permettant les accès à la base de données
	 */
	private PersonneDAO maPersonneDAO;
	
	/**
	 * Constructeur
	 * Définit la fenêtre et ses composants - affiche la fenêtre
	 */
	public PersonneFenetre()
    {
		// on instancie la classe Article DAO
		this.maPersonneDAO = new PersonneDAO();
		
		//on fixe le titre de la fenêtre
		this.setTitle("Personne");
		//initialisation de la taille de la fenêtre
		this.setSize(400,350);
		
		//création du conteneur
		containerPanel = new JPanel();
		
		//choix du Layout pour ce conteneur
		//il permet de gérer la position des éléments
		//il autorisera un retaillage de la fenêtre en conservant la présentation
		//BoxLayout permet par exemple de positionner les élements sur une colonne ( PAGE_AXIS )
		containerPanel.setLayout(new BoxLayout(containerPanel, BoxLayout.PAGE_AXIS));
		
		//choix de la couleur pour le conteneur
        containerPanel.setBackground(Color.YELLOW);
        
        
		//instantiation des  composants graphiques
		textFieldNom=new JTextField();
		textFieldPrenom=new JTextField();
		
		boutonEnvoi=new JButton("envoyer");
		boutonAffichageTousLesPersonnes=new JButton("afficher toutes les personnes");
		labelId=new JLabel("L'Id sera générée par la base de données");
		labelNom=new JLabel("Nom :");
		labelPrenom=new JLabel("Prenom :");
		
        zoneTextListPersonnes = new JTextArea(5, 20);
        zoneDefilement = new JScrollPane(zoneTextListPersonnes); 
        zoneTextListPersonnes.setEditable(false);     
       
		
		
		//ajout des composants sur le container 
		containerPanel.add(labelNom);
		//introduire une espace constant entre le label et le champ texte
		containerPanel.add(Box.createRigidArea(new Dimension(0,5)));
		containerPanel.add(textFieldNom);
		//introduire une espace constant entre le champ texte et le composant suivant
		containerPanel.add(Box.createRigidArea(new Dimension(0,10)));

		containerPanel.add(labelPrenom);
		containerPanel.add(Box.createRigidArea(new Dimension(0,5)));
		containerPanel.add(textFieldPrenom);
		containerPanel.add(Box.createRigidArea(new Dimension(0,10)));

	
		containerPanel.add(boutonEnvoi);
		containerPanel.add(Box.createRigidArea(new Dimension(0,5)));
		containerPanel.add(labelId);
		
		containerPanel.add(Box.createRigidArea(new Dimension(0,20)));

		containerPanel.add(boutonAffichageTousLesPersonnes);
		containerPanel.add(Box.createRigidArea(new Dimension(0,5)));
		containerPanel.add(zoneDefilement);
		
		//ajouter une bordure vide de taille constante autour de l'ensemble des composants
		containerPanel.setBorder(BorderFactory.createEmptyBorder(10,10,10,10));
		
		//ajout des écouteurs sur les boutons pour gérer les évènements
		boutonEnvoi.addActionListener(this);
		boutonAffichageTousLesPersonnes.addActionListener(this);
		
		//permet de quitter l'application si on ferme la fenêtre
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		this.setContentPane(containerPanel);

		//affichage de la fenêtre
		this.setVisible(true);
	}
	
	/**
	 * Gère les actions réalisées sur les boutons
	 *
	 */
	public void actionPerformed(ActionEvent ae)
	{
		int retour; // code de retour de la classe ArticleDAO
		
		try {
			if(ae.getSource()==boutonEnvoi)
			{
				//on crée l'objet message
				Personne a=new Personne(this.textFieldNom.getText(), this.textFieldPrenom.getText());
				//on demande à la classe de communication d'envoyer l'article dans la table article
				retour = maPersonneDAO.ajouter(a);
				// affichage du nombre de lignes ajoutées
				// dans la bdd pour vérification
				System.out.println("" + retour + " ligne ajoutée ");
			}
			else if(ae.getSource()==boutonAffichageTousLesPersonnes)
			{
				// on demande à la classe ArticleDAO d'ajouter le message
				// dans la base de données
				List<Personne> liste = maPersonneDAO.getListePersonnes();
			
				//on affiche dans la console du client les articles reçus
				for(Personne a : liste)
				{
					 zoneTextListPersonnes.append(a.toString());
				     zoneTextListPersonnes.append("\n");
					//Pour afficher dans la console : System.out.println(a.toString());	
				}
			}
		}
		catch (Exception e) {
			System.err.println("Veuillez contrôler vos saisies");
		}
		
	}

	
	public static void main(String[] args)
	{
		new PersonneFenetre();
    }

	


}
